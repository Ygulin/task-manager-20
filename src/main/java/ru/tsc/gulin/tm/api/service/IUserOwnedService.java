package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.api.repository.IUserOwnedRepository;
import ru.tsc.gulin.tm.enumerated.Sort;
import ru.tsc.gulin.tm.model.AbstractUserOwnedModel;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {
}
