package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.model.Task;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    Task create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

}
