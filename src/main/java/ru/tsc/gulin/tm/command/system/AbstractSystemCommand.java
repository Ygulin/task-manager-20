package ru.tsc.gulin.tm.command.system;

import ru.tsc.gulin.tm.api.service.ICommandService;
import ru.tsc.gulin.tm.command.AbstractCommand;
import ru.tsc.gulin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
