package ru.tsc.gulin.tm.command.user;

import ru.tsc.gulin.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String NAME = "logout";

    public static final String DESCRIPTION = "Logout user";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}
